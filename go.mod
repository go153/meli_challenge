module gitlab.com/Jota_cepeda/meli_challenge

go 1.15

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	go.mongodb.org/mongo-driver v1.9.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/text v0.3.7 // indirect
)
