package handler

import (
	"net/http"

	processor "gitlab.com/Jota_cepeda/meli_challenge/internal/mutant"
	statprocessor "gitlab.com/Jota_cepeda/meli_challenge/internal/stat"
	"gitlab.com/Jota_cepeda/meli_challenge/tools"
)

// CreateMutantHandler - POST - /api/v1/Mutant
func CreateMutantHandler(w http.ResponseWriter, r *http.Request) {

	status, resp, err := processor.CreateMutantProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err.Error(), 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, status)
	return
}

// STAT
// GetAllStatByMutantHandler - GET - /api/v1/stat
func GetAllStatByMutantHandler(w http.ResponseWriter, r *http.Request) {

	resp, err := statprocessor.GetAllStatByMutantProcessor(w, r)
	if err != nil {
		tools.HandlersResponse(w, nil, err, 500)
		return
	}

	tools.HandlersResponse(w, resp, nil, 200)
}
