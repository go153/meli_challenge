package handler_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	handler "gitlab.com/Jota_cepeda/meli_challenge/internal/handler"
)
// Test_CreateMutantHandler_error ...
func Test_CreateMutantHandler_error(t *testing.T) {

	payload := `{
		"dna": [
			"ATGCGA",
			"CAGTGC",
			"TTATGT",
			"AGAAGG",
			"CCCCTA",
			"TCACTW"
		]
	}`

	wsPayload := strings.NewReader(payload)
	req := httptest.NewRequest(http.MethodPost, "/api/v1/mutant", wsPayload)
	w := httptest.NewRecorder()
	handler.CreateMutantHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	fmt.Println("DATA RESULT ::: " + string(data))

}
// Test_CreateMutantHandler ...
func Test_CreateMutantHandler(t *testing.T) {

	payload := `{
		"dna": [
			"ATGCGA",
			"CAGTGC",
			"TTATGT",
			"AGAAGG",
			"CCCCTA",
			"TCACTG"
		]
	}`

	wsPayload := strings.NewReader(payload)
	req := httptest.NewRequest(http.MethodPost, "/api/v1/mutant", wsPayload)
	w := httptest.NewRecorder()
	handler.CreateMutantHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	fmt.Println("DATA RESULT ::: " + string(data))

}



// Test_GetAllStatByMutantHandler ...
func Test_GetAllStatByMutantHandler(t *testing.T) {

	req := httptest.NewRequest(http.MethodGet, "/api/v1/stat", nil)
	w := httptest.NewRecorder()
	handler.GetAllStatByMutantHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	fmt.Println("DATA RESULT ::: " + string(data))

}

// Test_GetAllStatByMutantHandler_error ...
func Test_GetAllStatByMutantHandler_error(t *testing.T) {

	req := httptest.NewRequest(http.MethodGet, "/api/v1/stat", nil)
	w := httptest.NewRecorder()
	handler.GetAllStatByMutantHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	fmt.Println("DATA RESULT ::: " + string(data))

}
