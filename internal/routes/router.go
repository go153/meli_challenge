package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/Jota_cepeda/meli_challenge/internal/handler"
)

func Router(r *mux.Router) {
	// MUTANT ...
	r.HandleFunc("/api/v1/mutant", handler.CreateMutantHandler).Methods("POST")
	// STAT ...
	r.HandleFunc("/api/v1/stat", handler.GetAllStatByMutantHandler).Methods("GET")
}
