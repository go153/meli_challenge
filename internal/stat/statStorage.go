package mutant

import (
	"fmt"
	"os"
	"strings"
)

var statStorage statStorageInterface

func init() {
	setMessageStorage()
}

type statStorageInterface interface {
	GetAllStat() (Stat, error)
}

func setMessageStorage() {

	dbEngine := os.Getenv("DB_ENGINE")

	switch strings.ToLower(dbEngine) {
	case "mongodb":
		statStorage = statMongoDB{}
	case "postgres":
		fallthrough
	case "oci8":
		fallthrough
	default:
		fmt.Printf("este motor de bd no está configurado aún: %s ", os.Getenv("DB_CONNECTION"))
	}

}
