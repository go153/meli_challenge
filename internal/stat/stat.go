package mutant

// User model
type Stat struct {
	Ratio         float64 `json:"ratio,omitempty" bson:"ratio"`
	CountHumanDna int64   `json:"count_human_dna,omitempty" bson:"count_human_dna"`
	CountMutanDna int64   `json:"count_mutant_dna,omitempty" bson:"count_mutant_dna"`
}
