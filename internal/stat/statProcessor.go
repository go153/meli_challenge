package mutant

import (
	"net/http"
)

//Message

// GetAllStatByMutantProcessor ...
func GetAllStatByMutantProcessor(w http.ResponseWriter, r *http.Request) (Stat, error) {

	results := Stat{}
	user, err := statStorage.GetAllStat()
	if err != nil {
		return results, err
	}

	return user, nil
}
