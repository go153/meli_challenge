package mutant

import (
	"context"
	"fmt"
	"os"

	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type statMongoDB struct{}

var (
	statdataBaseName   string = "dna"
	statCollectionName string = "mutants"
	client             *mongo.Client
)

// init ...
func init() {
	var err error
	client, err = getConnection()
	if err != nil {
		log.Fatalln(err)
	}
}

// getConnection ...
func getConnection() (*mongo.Client, error) {

	stringConnection := os.Getenv("DB_STRING_CONNECTION")

	clientOpts := options.Client().ApplyURI(fmt.Sprintf(stringConnection))

	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		return nil, err
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// GetAllStat ...
func (s statMongoDB) GetAllStat() (Stat, error) {

	results := Stat{}
	messageDB, err := messageWorkMongoDBGetAllInstanceData()
	if err != nil {
		return results, err
	}

	return messageDB, nil
}

// messageWorkMongoDBGetAllInstanceData ...
func messageWorkMongoDBGetAllInstanceData() (Stat, error) {

	results := Stat{}
	collection := client.Database(statdataBaseName).Collection(statCollectionName)

	filter_mutants := bson.D{{Key: "count_mutant_dna", Value: 1}}
	count_mutants, err := collection.CountDocuments(context.TODO(), filter_mutants)
	if err != nil {
		return results, err
	}

	filter_humans := bson.D{{Key: "count_human_dna", Value: 1}}
	count_humans, err := collection.CountDocuments(context.TODO(), filter_humans)
	if err != nil {
		return results, err
	}

	results.CountMutanDna = count_mutants
	results.CountHumanDna = count_humans
	results.Ratio = float64(count_mutants) / float64(count_humans)

	if float64(count_humans) == 0 {
		results.Ratio = float64(count_mutants)
	}

	return results, nil
}
