package mutant

import (
	"context"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mutantMongoDB struct{}

var (
	dataBaseName   string = "dna"
	collectionName string = "mutants"
	client         *mongo.Client
)

func init() {
	var err error
	client, err = getConnection()
	if err != nil {
		log.Fatalln(err)
	}
}

// InsertMutant ...
func (s mutantMongoDB) InsertMutant(data *Mutant) error {

	err := mutantWorkMongoDBInsertData(data)
	if err != nil {
		return err
	}

	return nil
}

// getConnection ...
func getConnection() (*mongo.Client, error) {

	stringConnection := os.Getenv("DB_STRING_CONNECTION")

	clientOpts := options.Client().ApplyURI(fmt.Sprintf(stringConnection))

	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		return nil, err
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// mutantWorkMongoDBInsertData ...
func mutantWorkMongoDBInsertData(data *Mutant) error {

	collection := client.Database(dataBaseName).Collection(collectionName)

	_, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		return err
	}

	return nil
}
