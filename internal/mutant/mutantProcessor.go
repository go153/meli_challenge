package mutant

import (
	"encoding/json"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/Jota_cepeda/meli_challenge/tools"
)

// CreateMutantProcessor ...
func CreateMutantProcessor(w http.ResponseWriter, r *http.Request) (int, string, error) {

	Mutant := &Mutant{}

	if err := json.NewDecoder(r.Body).Decode(Mutant); err != nil {
		return 403, "no mutant", err
	}

	Mutant.CountMutanDna = 0
	Mutant.CountHumanDna = 1
	Mutant.ID = uuid.New().String()

	res, err := tools.IsMutant(Mutant.DNA)
	if err != nil {
		return 403, "no mutant", err
	}

	if res {
		Mutant.CountMutanDna = 1
		Mutant.CountHumanDna = 0
	}

	err = mutantStorage.InsertMutant(Mutant)
	if err != nil {
		return 403, "no mutant", err
	}
	status := 403
	data := "Dna is of not a Mutant"
	if res {
		status = 200
		data = "Dna is of a Mutant"
	}
	return status, data, nil
}
