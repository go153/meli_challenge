package mutant

// User model
type Mutant struct {
	ID            string   `json:"_id,omitempty" bson:"_id"`
	DNA           []string `json:"dna,omitempty" bson:"dna"`
	CountHumanDna int      `json:"count_human_dna,omitempty" bson:"count_human_dna"`
	CountMutanDna int      `json:"count_mutant_dna,omitempty" bson:"count_mutant_dna"`
}
 