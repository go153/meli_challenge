package mutant

import (
	"fmt"
	"os"
	"strings"
)

var mutantStorage MutantStorageInterface

func init() {
	setMutantStorage()
}

type MutantStorageInterface interface {
	InsertMutant(Mutant *Mutant) error
}

// setMutantStorage ...
func setMutantStorage() {

	dbEngine := os.Getenv("DB_ENGINE")

	switch strings.ToLower(dbEngine) {
	case "mongodb":
		mutantStorage = mutantMongoDB{}
	case "postgres":
		fallthrough
	case "oci8":
		fallthrough
	default:
		fmt.Printf("este motor de bd no está configurado aún: %s", os.Getenv("DB_CONNECTION"))
	}

}
