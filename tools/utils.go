package tools

import (
	"errors"
)

// isMutant ...
func IsMutant(dna []string) (bool, error) {

	matrix, matrix_oblicuo, err := validation_createMartiz(dna)
	if err != nil {
		return false, err
	}

	count_dna_mutant := 0
	for _, value := range matrix {
		res, err := inARow(string(value))
		if err != nil {
			return false, err
		}
		count_dna_mutant = count_dna_mutant + res
	}

	value_oblicuo := ""
	for j := 0; j < len(matrix); j++ {
		value := ""
		for i := 0; i < len(matrix); i++ {
			a_value := string(matrix[i][j])
			value = value + string(a_value)
		}
		res_vertical, err := inARow(value)
		if err != nil {
			return false, err
		}
		count_dna_mutant = count_dna_mutant + res_vertical
		value_oblicuo = value_oblicuo + string(matrix_oblicuo[j])
	}

	res, err := inARow(value_oblicuo)
	if err != nil {
		return false, err
	}
	count_dna_mutant = count_dna_mutant + res

	if count_dna_mutant > 1 {
		return true, nil
	}
	return false, nil
}

// validation_createMartiz ...
func validation_createMartiz(data []string) ([][]uint8, [][]uint8, error) {
	var matrices [][]uint8
	var matrices_Oblicuo [][]uint8

	for i := 0; i < len(data); i++ {

		if len(data) != len(data[i]) {
			return nil, nil, errors.New("The matrix of dna must be NxN")
		}

		if !validateValue(data[i]) {
			return nil, nil, errors.New("Only the value of A,T,C,G is allowed in the dna")
		}

		a := []uint8{}
		for j := 0; j < len(data[i]); j++ {
			a_value := data[i][j]
			a = append(a, a_value)
		}

		matrices = append(matrices, a)
		b := []uint8{data[i][i]}
		matrices_Oblicuo = append(matrices_Oblicuo, b)
	}
	return matrices, matrices_Oblicuo, nil
}

// inARow ...
func inARow(data string) (int, error) {
	count_dna_mutant := 0
	sequence := 4 //os.Getenv("DB_STRING_CONNECTION")
	//sequence, err := strconv.Atoi(seq)
	// if err != nil {
	// 	return 0, errors.New("Is necesary config secuence")
	// }

	if len(data) >= sequence {
		for j := 0; j < len(data)-(sequence-1); j++ {

			bool_dna_mutant := true

			valor := []string{}
			count_sequence := 0
			for i := 0; i < len(data); i++ {

				valor = append(valor, string(data[i]))
				if bool_dna_mutant {
					if count_sequence == 4 {
						continue
					}
					if data[j] == data[i] {
						count_sequence++
					} else {
						bool_dna_mutant = false
					}
				}

			}

			if bool_dna_mutant {
				return count_dna_mutant + 1, nil
			}
		}
	}

	return count_dna_mutant, nil
}

// validateValue ...
func validateValue(data string) bool {
	dna_type := []string{"A", "T", "C", "G"}
	for j := 0; j < len(data); j++ {
		if !contains(dna_type, string(data[j])) {
			return false
		}
	}

	return true
}

// contains ...
func contains(elems []string, v string) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}
