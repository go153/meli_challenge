# meli_challenge
## Getting started LOCAL
go mod init
go mod download
source .env
go build ./...
go run .

## Add reposite
https://gitlab.com/go153/meli_challenge

## Integrate with heroku
- https://melichallenge2022.herokuapp.com/api/v1/mutant
- https://melichallenge2022.herokuapp.com/api/v1/stat

## Integrate with MongoDb
- https://cloud.mongodb.com/

## Test 
go test ./... -coverprofile cover.out
go tool cover -html=cover.out
***

## Support
Jonathan Cepeda ,  Jonathan.javier.cepeda@gmail.com

## Authors and acknowledgment
Jonathan Cepeda
